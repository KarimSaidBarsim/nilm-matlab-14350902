function [ detectionOut ] = eventDetectionDBSCAN( detectionSignals, varargin )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% EVENTDETECTIONDBSCAN is function to detect events/transients in a signal using the Density-Based Clustering
%   scheme for Applications with Noise (DBSCAN).
%
% SYNOPSIS: 
%
%   INPUT PARAMTERS [REQUIRED]:
%   - detectionSignals  : [N-by-M matrix ... M is the number of variables & N is the number of samples]
%   - timeVector        : [N-by-1 Vector ... JAVA TIMESTAMP]
%   - evDetDBSCANConfig : configuration inputs
%
%   EXAMPLES
%
% created by: Karim Said Barsim
% DATE: 21-Apr-2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%#ok<*AGROW>
% TODO: Consider only JAVA Timestamps
% TODO: Provide support for data gaps.
% TODO: Internal radius estimation estimation :
                             % Select random segment of the input signal (preferably during nights).
                              % Sweep the bin sizes from a very small value eps --> to a very large value ...
                              % ... (maxP - minP), (maxQ - minQ).
                              % Make a convolution between all curves.
                              % Infer the selected radii from the absolute maxima of the convoluted curves.

%% ============================================================================================================ Function Constants ...
% In this section all constants are declared and assigned. ANY MODIFICATION TO THESE VARIABLES OUTSIDE THIS
% SECTION IS A LOGIC ERROR.
TIC_ID                      = tic;                                                                              % Used for monitoring the performance of the function.
FILE_NAME                   = mfilename;                                                                        % Used for more informative error messages.
WAITBAR_STRING              = 'Event detection (DBSCAN) ...';                                                   % A string for the multiWaitbar function.
FUNCTION_START_TIME         = now;                                                                              % Used for reporting to the command line.

%% ============================================================================================================ Parsing input parameters ...
%-------------------------------------------------------------------------------------------------------------- Processing ...
% In this section all input parameters are parsed and assigned. All identifiers of the input parameters start
% with 'in_*'. ANY MODIFICATION TO THESE VARIABLES OUTSIDE THIS SECTION IS A LOGIC ERROR UNLESS MARKED WITH A
% COMMENT LIKE 'EXCEPTIONAL ASSIGNMENT'

in_eps                       = 15;                                                                              % Normally in <Watts,vars,VA,...>.
in_minPts                    = 1.2 * 50;                                                                        % Samples (i.e. 1.5 seconds) (the minimum length/duration of a cluster/steady-state)
in_eventWindow               = 30 * 50;                                                                         % Samples (i.e. 1/2 minute).
in_detectionStep             = 1 * 50;                                                                          % Samples (i.e. detection step of 1 second).
in_clusterDensity            = 0.25;
in_functionStartTime         = FUNCTION_START_TIME;                                                             % TODO: DOCUMENTATION. THEN NEED FOR THIS PARAMETER IS EXPLAINED LATER.
in_isCMD                     = true;                                                                            % Reporting progress to the command line.
in_isWaitbar                 = false;                                                                           % Showing a wait bar during processing.

i = 1;
while( i <= numel(varargin) )
    if(isa(varargin{i},'evDetDBSCANConfig'))
        in_eps               = varargin{i}.eps              ;
        in_minPts            = varargin{i}.minPts           ;
        in_eventWindow       = varargin{i}.eventWindow      ;
        in_detectionStep     = varargin{i}.detectionStep    ;
        in_clusterDensity    = varargin{i}.clusterDensity   ;
        i = i + 1;
    else
        switch lower(varargin{i})
            case {'eps'              }, in_eps               = varargin{i + 1}; i = i + 2;
            case {'minpts'           }, in_minPts            = varargin{i + 1}; i = i + 2;
            case {'eventwindow'      }, in_eventWindow       = varargin{i + 1}; i = i + 2;
            case {'detectionstep'    }, in_detectionStep     = varargin{i + 1}; i = i + 2;
            case {'clusterdensity'   }, in_clusterDensity    = varargin{i + 1}; i = i + 2;
            case {'functionstarttime'}, in_functionStartTime = varargin{i + 1}; i = i + 2;
            case {'iscmd'            }, in_isCMD             = varargin{i + 1}; i = i + 2;
            case {'iswaitbar'        }, in_isWaitbar         = varargin{i + 1}; i = i + 2;
            otherwise,error([FILE_NAME ':InvalidPropertyName'], [FILE_NAME ...
                            ' -> ERROR : Invalid property name ... ''' varargin{i} '''']);
        end
    end
end

if(isscalar(in_eps))
    in_eps = in_eps(1,ones(1,size(detectionSignals,2)));
end

%-------------------------------------------------------------------------------------------------------------- Validation ...
validateattributes(detectionSignals    ,{'numeric'},{'2d','nonnan','real'                            },FILE_NAME,'InputData'        );
validateattributes(in_eps              ,{'numeric'},{'vector','finite','nonnan','positive','real'    },FILE_NAME,'Eps'              );
validateattributes(in_minPts           ,{'numeric'},{'vector','finite','nonnan','positive','integer' },FILE_NAME,'MinPts'           );
validateattributes(in_eventWindow      ,{'numeric'},{'scalar','finite','nonnan','positive','integer' },FILE_NAME,'EventWindow'      );
validateattributes(in_detectionStep    ,{'numeric'},{'scalar','finite','nonnan','positive','integer' },FILE_NAME,'DetectionStep'    );
validateattributes(in_clusterDensity   ,{'numeric'},{'scalar','finite','nonnan','positive','real'    },FILE_NAME,'ClusterDensity'   );
validateattributes(in_functionStartTime,{'numeric'},{'scalar','finite','nonnan'                      },FILE_NAME,'FunctionStartTime');
validateattributes(in_isCMD            ,{'logical'},{'scalar'                                        },FILE_NAME,'IsCMD'            );
validateattributes(in_isWaitbar        ,{'logical'},{'scalar'                                        },FILE_NAME,'IsWaitbar'        );
assert(size(detectionSignals,2) == size(in_eps,2) , [FILE_NAME ' -> ERROR : Inconsistent dimensions ... InputData & Eps .']);
assert(isequal(size(in_minPts),size(in_eventWindow)) & isequal(size(in_minPts),size(in_detectionStep)), ...
        [FILE_NAME ' -> ERROR : Inconsistent detection levels.']);

%-------------------------------------------------------------------------------------------------------------- Command line ...
if in_isCMD
    disp('========================================== Event Detection (DBSCAN) ==========================================');
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : Please verify the following input parameters ... '                           ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : SIZE(InputData)   = ' num2str(size(detectionSignals))                            ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : Eps               = ' num2str(in_eps)                                        ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : MinPts            = ' num2str(in_minPts)                                     ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : EventWindow       = ' num2str(in_eventWindow)                                ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : DetectionStep     = ' num2str(in_detectionStep)                              ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : ClusterDensity    = ' num2str(in_clusterDensity)                             ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : FunctionStartTime = ' datestr(in_functionStartTime,'yyyy-mm-dd HH:MM:SS.FFF')]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : IsCMD             = ' num2str(in_isCMD)                                      ]);
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : IsWaitbar         = ' num2str(in_isWaitbar)                                  ]);
    disp('--------------------------------------------------------------------------------------------------------------');
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : Started at ... ' datestr(now,'yyyy-mm-dd HH:MM:SS.FFF')                      ]);
end
if in_isWaitbar
    multiWaitbar(WAITBAR_STRING,'Reset','Color', [0.0 0.8 0.0]);
end

%% ============================================================================================================ Processing ...
%-------------------------------------------------------------------------------------------------------------- Initialization ...
fixedEndLimit    = size(detectionSignals,1) - in_detectionStep;
coarseTransients = [];      fineTransients    = [];     problematicEvents  = [];
isLeftSuccessful = [];      isRightSuccessful = [];     isCoarseSuccessful = [];
detectionCounter = 1;       fixedStartPoint   = 1;      primaryEndPoint    = 1;
detectionDiffs   = [];

%-------------------------------------------------------------------------------------------------------------- Detection ...
while primaryEndPoint <= fixedEndLimit                                                                          % Sliding window
    if in_isWaitbar, multiWaitbar(WAITBAR_STRING, primaryEndPoint / fixedEndLimit); end                         % Progress indication
    %---------------------------------------------------------------------------------------------------------- Forward detection ...
    clear dbscan;
    for primaryEndPoint = (fixedStartPoint + in_detectionStep):in_detectionStep:size(detectionSignals,1)                  % Forward detection
        primaryStartPoint = max([fixedStartPoint (primaryEndPoint - in_eventWindow)]);                          % Limit the detection window
        MOE = dbscan(detectionSignals(primaryStartPoint:primaryEndPoint,:),in_minPts(1), in_eps(1,:), primaryStartPoint, primaryEndPoint);              % Perform clustering
        %figure(1); plot(detectionSignals(primaryStartPoint:primaryEndPoint,1),'r.-'); a1 = gca;
        %figure(2); stem(MOE); a2 = gca; linkaxes([a1,a2],'x');
        if max(MOE) > 1                                                                                         % If more than one cluster is detected.
            MOE = denserOverlappingClusters(MOE, 0.33);
            MOE = reorderClustersIndices(MOE);
            MOE = removeScatteredClusters(MOE, in_clusterDensity);
            clusterIDs = unique(MOE(MOE > 0));                                                                  % Get clusters' IDs after density check.
            %figure(1); plot(detectionSignals(primaryStartPoint:primaryEndPoint,1),'r.-'); a1 = gca;
            %figure(2); stem(MOE); a2 = gca; linkaxes([a1,a2],'x');
            if(numel(clusterIDs) > 1)                                                                           % If the number of clusters is still more than 1 ...
                MOE = dbscan(detectionSignals(primaryStartPoint:primaryEndPoint,:),in_minPts(1), in_eps(1,:), primaryStartPoint, primaryEndPoint);              % Perform clustering
                MOE = denserOverlappingClusters(MOE, 0.33);
                MOE = reorderClustersIndices(MOE);
                MOE = removeScatteredClusters(MOE, in_clusterDensity);
                clusterIDs = unique(MOE(MOE > 0));                                                                  % Get clusters' IDs after density check.
                %figure(1); plot(detectionSignals(primaryStartPoint:primaryEndPoint,1),'r.-'); a1 = gca;
                %figure(2); stem(MOE); a2 = gca; linkaxes([a1,a2],'x');
                break;                                                                                          % ... then forward detection ended.
            end
        end
    end
    %---------------------------------------------------------------------------------------------------------- Forward detection validation ...
    if numel(clusterIDs) <= 1                                                                                   % If exited with only one cluster or less ...
        break;                                                                                                  % ... then that is the end of the detection process.
    elseif numel(clusterIDs) == 2                                                                               % If exited normally (two clusters are detected).
        isRightSuccessful(detectionCounter) = true;                                                             % New right detection, level 1.
    else                                                                                                        % If more than two clusters are detected ...
        warning([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME,...
                ' : WARNING -> Clusters counter = ',num2str(numel(clusterIDs)),...
                ', Range = ',num2str(primaryStartPoint),':',num2str(primaryEndPoint)]);
        isRightSuccessful(detectionCounter) = false;                                                            % ... then it is an unsuccessful first detection unless ...
        for secondaryEndPoint = (primaryEndPoint-1):-1:(primaryEndPoint-in_detectionStep+1)                     % go backward step by step
            MOE = dbscan(detectionSignals(primaryStartPoint:secondaryEndPoint,:),in_minPts(1), in_eps(1,:));    % Perform DBSCAN Clustering
            if max(MOE) == 2                                                                                    % If two clusters are detected ...
                primaryEndPoint = secondaryEndPoint;                                                            % ... change the forward detection end point
                isRightSuccessful(detectionCounter) = true;                                                     % ... because it is now a successful forward detection.
                break;
            end
        end
    end
    %---------------------------------------------------------------------------------------------------------- Forward detection further validation ...
    % TODO : Define criteria to select only two clusters.
    %---------------------------------------------------------------------------------------------------------- Backward detection further validation ...
    fixedStartPoint = primaryStartPoint;
    clear dbscan
    for primaryStartPoint = fixedStartPoint:in_detectionStep:primaryEndPoint                                    % Backward detection
        MOE = dbscan(detectionSignals(primaryStartPoint:primaryEndPoint,:),in_minPts(1), in_eps(1,:), primaryStartPoint, primaryEndPoint);          % Perform DBSCAN Clustering
        MOE = denserOverlappingClusters(MOE, 0.33);
        MOE = reorderClustersIndices(MOE);
        MOE = removeScatteredClusters(MOE, in_clusterDensity);
        %figure(1); plot(detectionSignals(primaryStartPoint:primaryEndPoint,1),'r.-'); a1 = gca;
        %figure(2); stem(MOE); a2 = gca; linkaxes([a1,a2],'x');
        if max(MOE) < 2                                                                                         % Until only one cluster is detected.
            break;
        end
    end
    primaryStartPoint = max([fixedStartPoint, (primaryStartPoint - in_detectionStep)]);                            % Go back by one detection step.
    %---------------------------------------------------------------------------------------------------------- Backward detection further validation ...
    % TODO : Define criteria to select only two clusters.
    %---------------------------------------------------------------------------------------------------------- Ending the Coarse detection ...
    MOE = dbscan(detectionSignals(primaryStartPoint:primaryEndPoint,:),in_minPts(1), in_eps(1,:));                  % Perform DBSCAN clustering (TODO: A chance for caching here even though it is not the performance bottle-neck).
    MOE = denserOverlappingClusters(MOE, 0.33);
    MOE = reorderClustersIndices(MOE);
    MOE = removeScatteredClusters(MOE, in_clusterDensity);
    isLeftSuccessful(detectionCounter)   = max(MOE) == 2;                                                       % Normal/successful coarse detection.
    isCoarseSuccessful(detectionCounter) = isLeftSuccessful(detectionCounter) || isRightSuccessful(detectionCounter);
    if ~isLeftSuccessful(detectionCounter) && isRightSuccessful(detectionCounter)                               % Event limits' selection based on successful detections
        finalCoarseLimits = [fixedStartPoint, primaryEndPoint];
    else
        finalCoarseLimits = [primaryStartPoint, primaryEndPoint];
    end
    %---------------------------------------------------------------------------------------------------------- Detection refinement ...
    if isCoarseSuccessful(detectionCounter)                                                                     % Detection refinement is only possible if coarse detection was successful
        tempSegment = detectionSignals(finalCoarseLimits(1):finalCoarseLimits(2),:);                                % The event segment.
        MOE = dbscan(tempSegment,in_minPts(1), in_eps(1,:));                                            % Perform DBSCAN clustering (TODO: A chance for caching here even though it is not the performance bottle-neck).
        MOE = denserOverlappingClusters(MOE, 0.33);
        MOE = reorderClustersIndices(MOE);
        MOE = removeScatteredClusters(MOE, in_clusterDensity);
        MOE = reorderClustersIndices(MOE);
        if max(MOE) ~= 2                                                                                        % If it is unsuccessful ...
            warning('fff');
            problematicEvents   = [problematicEvents; finalCoarseLimits];                                       % ... save it in different output
            fixedStartPoint     = ceil(mean(finalCoarseLimits));                                                % ... continue and ignore current detection.
            continue;
        end
        clusterAInd = find(MOE == 1);                                                                           % Indices of the first cluster.
        clusterBInd = find(MOE == 2);                                                                           % Indices of the second cluster.
        if(mean(clusterAInd) >= mean(clusterBInd))                                                              % Determine which cluster is at the left/right of the other (TODO: better algorithms available).
            rightClusterInd = clusterAInd;
            leftClusterInd  = clusterBInd;
        else
            rightClusterInd = clusterBInd;
            leftClusterInd  = clusterAInd;
        end

        fineEndPoint   = rightClusterInd(find(rightClusterInd >= leftClusterInd(end-2), 2, 'first')) + finalCoarseLimits(1) - 1; % Selecting the event limits (TODO: better algorithms available).
        fineStartPoint = leftClusterInd( find(leftClusterInd  <= rightClusterInd(3) , 2, 'last' )) + finalCoarseLimits(1) - 1; % Selecting the event limits (TODO: better algorithms available).

        if(isempty(fineEndPoint) || isempty(fineStartPoint))                                                    % Check if new limits are accepted.
            clusterAInd     = leftClusterInd;
            leftClusterInd  = rightClusterInd;
            rightClusterInd = clusterAInd;
            fineEndPoint   = rightClusterInd(find(rightClusterInd >= leftClusterInd(end-2), 2, 'first')) + finalCoarseLimits(1) - 1;
            fineStartPoint = leftClusterInd( find(leftClusterInd  <= rightClusterInd(3) , 2, 'last' )) + finalCoarseLimits(1) - 1;
            if(isempty(fineEndPoint) || isempty(fineStartPoint))
                problematicEvents   = [problematicEvents; finalCoarseLimits];
                fixedStartPoint     = ceil(mean(finalCoarseLimits));
                warning('fff');
                continue;
            end
        end

        rVal = mean(tempSegment(rightClusterInd,:));
        lVal = mean(tempSegment(leftClusterInd ,:));
        currentDiff = rVal - lVal;                                                                              % Store value difference based only on coarse event detection.

        finalFineLimits = [fineStartPoint(1), fineEndPoint(end)];                                               % Add to detection output
    else
        problematicEvents   = [problematicEvents; finalCoarseLimits];
        fixedStartPoint     = ceil(mean(finalCoarseLimits));
        warning('fff');
        continue;
    end

    fixedStartPoint  = fineEndPoint(1);

    if size(fineTransients,1) > 0 && fixedStartPoint <= fineTransients(end,2)
        fixedStartPoint = finalCoarseLimits(2);
    end

    if size(coarseTransients,1) > 0 && finalCoarseLimits(2) <= coarseTransients(end,2)
        if(fixedStartPoint == size(detectionSignals,1))
            break;
        else
            fixedStartPoint = finalCoarseLimits(2) + 1;
            continue;
        end
    end

    if ~isCoarseSuccessful(detectionCounter)
        problematicEvents = [problematicEvents; finalCoarseLimits];
        fixedStartPoint   = ceil(mean(finalCoarseLimits));
        warning('fff');
        continue;
    end

    if finalFineLimits(2) == finalCoarseLimits(2)
        fixedStartPoint   = ceil(mean(finalCoarseLimits));
    end

    detectionDiffs   = [detectionDiffs; currentDiff];
    fineTransients   = [fineTransients; finalFineLimits];
    coarseTransients = [coarseTransients; finalCoarseLimits];
    detectionCounter = detectionCounter + 1;

    detectionOut.timing.perEventDetectionTimes(detectionCounter) = toc(TIC_ID);
    % --------------------------------------------------------------------------------------------------------- CMD info
    if in_isCMD
        disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : (' ...
              num2str(size(coarseTransients,1),'%03d') ...
              ') Coarse detection at [ ' num2str(finalCoarseLimits(1),'%09d') ' , ' ...
                                         num2str(finalCoarseLimits(2),'%09d') ' ]']);
    end
end

%if numel(isCoarseSuccessful) > size(coarseTransients,1)
%    isLeftSuccessful = isLeftSuccessful(1:(end-1));
%    isRightSuccessful = isRightSuccessful(1:(end-1));
%    isCoarseSuccessful = isCoarseSuccessful(1:(end-1));
%end
% ------------------------------------------------------------------------------------------------------------- CMD info
if in_isCMD
    switch size(coarseTransients,1)
        case 0
            disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ...
                ' : No events detected.']);
        case 1
            disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ...
                ' : One event detected.\n']);
        otherwise
            disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ...
                ' : A total of ' num2str(size(coarseTransients,1)) ' event detected.\n']);
    end
end
%% ============================================================================================================ Calculating steady states
% ------------------------------------------------------------------------------------------------------------- CMD info
if in_isCMD
    disp([datestr(now - in_functionStartTime,'HH:MM:SS.FFF') '-' FILE_NAME ' : Calculating steadies ...']);
end
% ------------------------------------------------------------------------------------------------------------- Processing ... (assuming the first and last segments are steady-states).
coarseSteadies = [];
fineSteadies   = [];
if ~isempty(coarseTransients)
    coarseSteadies  = [[1;(coarseTransients(:,2)+1)], [(coarseTransients(:,1)-1);size(detectionSignals,1)]];
    fineSteadies    = [[1;(fineTransients(:,2)+1)],   [(fineTransients(:,1)-1);size(detectionSignals,1)]];
end
%% ============================================================================================================ TODO: Second level detection (NOT YET AVAILABLE)
% -------------------------------------------------------------------------------------------------------------
% -------------------------------------------------------------------------------------------------------------
%% ============================================================================================================ Finishing
if in_isWaitbar, multiWaitbar(WAITBAR_STRING, 'Close'); end

detectionOut.timing.totalElapsedTime    = toc(TIC_ID);
detectionOut.timing.absoluteStartTime   = in_functionStartTime;
detectionOut.timing.info                = ['Please note that plotting, waiting, and ', ...
                                           'interactive context (if any) are all included.'];

detectionOut.inputs.eps               = in_eps;
detectionOut.inputs.minPts            = in_minPts;
detectionOut.inputs.eventWindow       = in_eventWindow;
detectionOut.inputs.detectionStep     = in_detectionStep;
detectionOut.inputs.clusterDensity    = in_clusterDensity;
detectionOut.inputs.functionStartTime = in_functionStartTime;
detectionOut.inputs.isCMD             = in_isCMD;
detectionOut.inputs.isWaitbar         = in_isWaitbar;

detectionOut.failureEvents            = problematicEvents;
detectionOut.coarseTransients         = coarseTransients;
detectionOut.coarseSteadies           = coarseSteadies;
detectionOut.fineTransients           = fineTransients;
detectionOut.fineSteadies             = fineSteadies;
detectionOut.detectionDiffs           = detectionDiffs;

detectionOut.steadyStates             = fineSteadies;
detectionOut.transients               = fineTransients;

end
