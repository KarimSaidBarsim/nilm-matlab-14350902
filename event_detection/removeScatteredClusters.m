function [ MOE ] = removeScatteredClusters( MOE, densityThreshold )
    for clusterIndex = 1:max(MOE)                                                                               % For each detected cluster/steady-state
        currrentMOECluster = (MOE == clusterIndex);
        if(( (sum(currrentMOECluster)-2) / ...
                (find(currrentMOECluster,2,'last')-find(currrentMOECluster,2,'first')) <= densityThreshold))    %TODO: 2nd first & 2nd last as end points here <<<<<<
                                                                                                                % Does it satisfy the clustering density requirements ?
            MOE(currrentMOECluster) = -1;                                                                       % If not, then it is noise.
        end
    end
end
