function [ MOE ] = mergeOverlappingClusters( MOE, overlappingRatio, freeClusterRatio )
    for firstClusterIndex = max(MOE):-1:1
        tempIndA = find(MOE == firstClusterIndex);
        for secondClusterIndex = (firstClusterIndex-1):-1:1
            tempIndB = find(MOE == secondClusterIndex);
            if( tempIndB(2) >= tempIndA(end-1) || tempIndA(2) >= tempIndB(end-1) )                              %TODO: 2nd first & 2nd last as end points here <<<<<<
                continue;
            end
            totalLength  = max([tempIndA(end-2),tempIndB(end-2)]) - min([tempIndA(2),tempIndB(2)]);             %TODO: 2nd first & 2nd last as end points here <<<<<<
            freeRight    = tempIndA(end-2) - tempIndB(end-2);                                                   %TODO: 2nd first & 2nd last as end points here <<<<<<
            freeLeft     = tempIndA(2) - tempIndB(2)    ;                                                           %TODO: 2nd first & 2nd last as end points here <<<<<<
            overlappingL = min([tempIndA(end-2),tempIndB(end-2)]) - max([tempIndA(2),tempIndB(2)]);             %TODO: 2nd first & 2nd last as end points here <<<<<<
            if( freeRight * freeLeft <= 0 || overlappingL > overlappingRatio * totalLength )
                MOE(MOE == firstClusterIndex) = secondClusterIndex;
                break;
            end
            if(nargin >= 3)
                freeAbsRigth = max([tempIndA(end-2),tempIndB(end-2)]) - min([tempIndA(end-2),tempIndB(end-2)]);
                freeAbsLeft  = max([tempIndA(2),tempIndB(2)]) - min([tempIndA(2),tempIndB(2)]);
                if( freeAbsRigth < freeClusterRatio * totalLength || freeAbsLeft < freeClusterRatio * totalLength )
                    MOE(MOE == firstClusterIndex) = secondClusterIndex;
                    break;
                end
            end
        end
    end
end
