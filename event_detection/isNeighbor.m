function [ neighbor ] = isNeighbor( ellipseCenter , ellipseRadii, targetPoints, isRelative)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ISNEIghbor ... TODO
%
% SYNOPSIS: [ funcLogicOut, funcTodoOut ] = ISNEIghbor( center, radii, target, key-value pairs )
%
%   INPUTS:
%   - ellipseCenter      : [ROW-VECTOR,REAL] the centre point of the ellipse. The vector length is M equal to
%                           the dimension of each target point.
%   - ellipseRadii       : [ROW-VECTOR,REAL] the radii of the ellipse. The vector length is M equal to
%                           the dimension of each target point.
%   - targetPoints       : [N-ROW VECTORS,REAL] an NxM matrix where each row is a point to check whether or not
%                           it lies within the test radius. N is the number of test points.
%   - isRelative         : [LOGIC] (default: false) if true, the radii will be interpreted as relative to
%                           the centre of the ellipse (i.e. For a centre point [X Y] the relative radii are
%                           [X*a Y*b])
%
%   OUTPUT:
%   - ellipsed           : [LOGICAL] if the 'point' is inside the ellipse, the function returns this output as
%                           true, otherwise it is false.
%   - relativeDistance   : [REAL] (TODO)
%
%   EXAMPLES: TODO
%       TESTED USAGE:  TODO
%
%   NOTES : 1) ellipseCenter, ellipseRadii, and targetPoints must all have the same dimension (i.e. same number
%                of columns).
%
%
% created by: Karim Said Barsim
% DATE: 20-Apr-2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ============================================================================================================ Parsing input parameters ...
%-------------------------------------------------------------------------------------------------------------- Processing ...
% In this section all input parameters are parsed and assigned. All identifiers of the input parameters start
% with 'in_*'. ANY MODIFICATION TO THESE VARIABLES OUTSIDE THIS SECTION IS A LOGIC ERROR UNLESS MARKED WITH A
% COMMENT LIKE 'EXCEPTIONAL ASSIGNMENT'
if nargin < 4
	isRelative = false;
end

%% ============================================================================================================ Processing ...

if isRelative
    computedRadii = ellipseCenter .* ellipseRadii;
    differences   = ((ellipseCenter(ones(size(targetPoints,1),1),:) - targetPoints) ./ ...
                                                        computedRadii(ones(size(targetPoints,1),1),:));
    neighbor      = nansum(differences.*differences,2) <= 1;
else
    computedRadii = ellipseRadii;
    differences   = ((ellipseCenter(ones(size(targetPoints,1),1),:) - targetPoints) ./ ...
                                                        computedRadii(ones(size(targetPoints,1),1),:));
    neighbor      = sum(differences.*differences,2) <= 1;
end


%% ============================================================================================================ Processing ...
%relativeDistance = inf; %TODO

end