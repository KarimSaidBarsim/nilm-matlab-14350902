function [ MOE ] = reorderClustersIndices( MOE )
    clusterIDs = unique(MOE);
    clusterIDs(clusterIDs <= 0) = [];
    for i = 1:numel(clusterIDs)
        if(clusterIDs(i) ~= i)
            MOE( MOE == clusterIDs(i) ) = i;
        end
    end
end