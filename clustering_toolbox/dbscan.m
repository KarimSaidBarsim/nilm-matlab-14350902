function [ clusterVector, classesVector, neighborsMatrix ] = dbscan( in_dataSamples, in_minPts, in_eps, in_startIndex, in_endIndex )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DBSCAN is a basic implementation of the Density-Based Clustering analysis for Applications with Noise
%   (DBSCAN). The distance query algorithm is Brute-force but featuring the matrix computation capabilities of
%   MATLAB. Also, if features the parallel computing of MATLAB.
%
% SYNOPSIS: [ CLUSTERVECTOR, CLASSESVECTOR, NEIGHBORSMATRIX ] = DBSCAN( IN_DATASAMPLES, IN_MINPTS, IN_EPS )
%
% All input parameters are required.
%   INPUT PARAMTERS :
%   - in_dataSamples     : [m-by-n MATRIX,REAL] The input data samples. M is the number of samples and each row
%                           is a different sample while N is the number of variables/dimensions and each column
%                           is a different variable.
%   - in_minPts          : [SCALAR,INTEGER] the minimum number of samples in a cluster (i.e. to select a core
%                           point of a cluster). Data samples which has minPts neighbours or more are cores and
%                           samples with less than minPts neighbours are either border points (if they are
%                           density-reachable from one of the core points) or noise.
%   - in_eps             : [1-by-n VECTOR,REAL] The radii/weights in each dimension for fixed radius near
%                           neighbour selection. (TODO: PROVIDE INTERNAL ESTIMATION OF THIS PARAMETER AND MAKE
%                           IT OPTIONAL).
%
%   INPUT PARAMTERS :
%   - clusterVector      : [m-by-1 VECTOR,REAL] A vector of cluster indices of data samples. Each element is
%                           either '-1' which is noise, 'c > 0' which the cluster index of the current sample.
%   - classesVector      : [m-by-1 VECTOR,REAL] A vector of sample types. Each element is either '-1' which is
%                           also noise, '0' which is a border point, or '1' which is a core point.
%   - neighborsMatrix    : [m-by-m VECTOR,LOGICAL] A square, identical matrix with all diagonal elements being
%                           true. Each element M(i,j) is true if ith and jth data samples are neighbours.
%
%
%   EXAMPLES :
%       DBSCAN( [1 2 3 4;2 3 4 5;6 5 3 7], 2, [2 2 3 2]);
%
%
%       TESTED USAGE: SEE EVENTDETECTIONDBSCAN
%
%
%   KNOWN LIMITATIONS :
%           1) Performance limits due to quadratic increase with number of samples (and Brute force query
%               algorithm).
%
%
% created by: Karim Said Barsim
% DATE: 12-Apr-2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%% ============================================================================================================ Memory pre-allocation ...
persistent prevNeighborsMatrix;
persistent prevStartIndex     ;
persistent prevEndIndex       ;
persistent prevMinPts         ;
persistent prevEps            ;

clusterVector   = zeros(size(in_dataSamples,1),1);
classesVector   = zeros(size(in_dataSamples,1),1); % (0:Noise), (0:Border), (1:Core)                            % (To check whether it is border or noise just query whether it belongs to a cluster or not).

%% ============================================================================================================ Fixed radius near neighbour ...
if(isempty(prevNeighborsMatrix) || isempty(prevStartIndex) || isempty(prevEndIndex) || nargin < 4 || ...
        in_startIndex > prevEndIndex || in_endIndex < prevStartIndex  || ...
        isempty(prevMinPts) || isempty(prevEps) || ~isequal(prevMinPts,in_minPts) || ~isequal(prevEps,in_eps) )
	prevNeighborsMatrix = cell(1,size(in_dataSamples,1));
    for i = 1:size(in_dataSamples, 1)
        prevNeighborsMatrix{i} = isNeighbor(in_dataSamples(i,:), in_eps, in_dataSamples, false);                        % PERFORMANCE BOTTLE-NECK
    end
    prevNeighborsMatrix = cell2mat(prevNeighborsMatrix);
else
    removalIndexes = [max([1, in_startIndex-prevStartIndex+1]), ...
                      size(prevNeighborsMatrix,1) + min([in_endIndex-prevEndIndex, 0])];
    prevNeighborsMatrix = prevNeighborsMatrix(removalIndexes(1):removalIndexes(2), removalIndexes(1):removalIndexes(2));
    
    M1 = cell(1,in_endIndex-prevEndIndex);
    curStartIndex = max([1,(prevStartIndex - in_startIndex + 1)]);
    curEndIndex = (prevEndIndex - in_startIndex + 1);
    for i = (prevEndIndex+1):in_endIndex
        M1{i-prevEndIndex} = isNeighbor(in_dataSamples(i-in_startIndex+1,:), in_eps, in_dataSamples(curStartIndex:curEndIndex,:), false);                        % PERFORMANCE BOTTLE-NECK
    end
    M1 = cell2mat(M1);

    M2 = cell(1,in_endIndex-prevEndIndex);
    for i = (prevEndIndex+1):in_endIndex
        M2{i-prevEndIndex} = isNeighbor(in_dataSamples(i-in_startIndex+1,:), in_eps, in_dataSamples((prevEndIndex - in_startIndex + 2):end,:), false);                        % PERFORMANCE BOTTLE-NECK
    end
    M2 = cell2mat(M2);

    M3 = cell(1,prevStartIndex-in_startIndex);
    for i = in_startIndex:(prevStartIndex-1)
        M3{i-in_startIndex+1} = isNeighbor(in_dataSamples(i-in_startIndex+1,:), in_eps, in_dataSamples((prevStartIndex - in_startIndex + 1):end,:), false);
    end
	M3 = cell2mat(M3);
    
    M4 = cell(1,prevStartIndex-in_startIndex);
    for i = in_startIndex:(prevStartIndex-1)
        M4{i-in_startIndex+1} = isNeighbor(in_dataSamples(i-in_startIndex+1,:), in_eps, in_dataSamples(1:(prevStartIndex - in_startIndex),:), false);        
    end
    M4 = cell2mat(M4);

    if(~isempty(M1))
        prevNeighborsMatrix = [prevNeighborsMatrix, M1];
        prevNeighborsMatrix = [prevNeighborsMatrix; [M1',M2]];
    end
    if(~isempty(M3)) 
        prevNeighborsMatrix = [M3, prevNeighborsMatrix];
        prevNeighborsMatrix = [[M4,M3']; prevNeighborsMatrix];
    end
end








neighborsSum = sum(prevNeighborsMatrix,1);
classesVector(neighborsSum >= in_minPts) = 1;  % Extracting all cores

%% ============================================================================================================ Core point detection ...
clusterCounter  = 0;
for i = 1:numel(clusterVector)
    if(clusterVector(i) == 0)
        clusterVector(i) = -1;
        if(classesVector(i))
            clusterCounter   = clusterCounter + 1;
            clusterVector(i) = clusterCounter;
            expandCluster(prevNeighborsMatrix(:,i) & (clusterVector <= 0));
        end
    end
end

%% ============================================================================================================ Expanding densities/clusters ...
    function [] = expandCluster( curIndices )
        clusterVector(curIndices) = clusterCounter;
        nextNeighbors = all(any(prevNeighborsMatrix(:,curIndices),2) & (clusterVector <= 0), 2);
        if(any(nextNeighbors))
            expandCluster(nextNeighbors);
        end
    end

neighborsMatrix = prevNeighborsMatrix;

if(nargin >= 4)
    prevStartIndex = in_startIndex;
    prevEndIndex = in_endIndex;      
    prevMinPts = in_minPts;
    prevEps = in_eps;
end
%% ============================================================================================================ End of function
end




%% ============================================================================================================ Alternative implementations for the algorithm bottle neck ... (tested)
% b = in_dataSamples(:,:,ones(size(in_dataSamples,1),1));
% c = in_eps(ones(size(in_dataSamples,1),1),:);
% c = c(:,:,ones(size(in_dataSamples,1),1));
% e = permute(sum(((permute(b,[3 2 1]) - b) ./ c).^2,2),[1 3 2]) <= 1;
% -------------------------------------------------------------------------------------------------------------
% e = squareform(pdist(in_dataSamples,@(x,y) curFunc(x,y)));
%     function [ curOut ] = curFunc( in1, in2 )
%         1;
%         curOut = isNeighbor(in1,in_eps,in2,false);
%         2;
%     end
% e(logical(eye(size(e)))) = 1;     OR      % e(1:(nRows+1):nRows*nRows) = 1;
% -------------------------------------------------------------------------------------------------------------
