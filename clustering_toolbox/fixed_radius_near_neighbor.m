function [ out_neighbors ] = fixed_radius_near_neighbor( in_data_vector, in_radii )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FIXED_RADIUS_NEAR_NEIGHBOR is an optimized algorithm for solving the fixed-radius near-neight specially for
% application with the DBSCAN and similar algorithms.
%
%--------------------------------------------------------------------------------------------------------------
% INPUTS:
%   - in_data_vector : [MxN Matrix,REAL] An MxN matrix where each column is a random variable (or a dimension)
%                       and each row is a data sample.
%   - in_radii       : [1xN Matrix,REAL] A vector of radii (normalization coeffecients or standard deviations)
%                       of each random variable in the N-dimensions.
%--------------------------------------------------------------------------------------------------------------
% OUTPUTS:
%   - out_neighbors  : [MxM Matrix,LOGIC] An MxM matrix of logic values each element m_ij is true iff both
%                       samples i and j lie within the provided radii in all dimensions.
%--------------------------------------------------------------------------------------------------------------
% NOTES:
%   - Standardized (normalized) Euclidean distances is used.
%--------------------------------------------------------------------------------------------------------------
% REQUIRES:
%   - Matlab's pdist function from statistics toolbox.
%--------------------------------------------------------------------------------------------------------------
% EXAMPLES:
%      - X = rand(1000, 3);
%        R = rand(1,    3);
%        S = FIXED_RADIUS_NEAR_NEIGHBOR(X,R);
%--------------------------------------------------------------------------------------------------------------
% created by: Karim Said Barsim
% DATE: 1435-09-02
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



temp_distances = pdist(in_data_vector, 'seuclidean', in_radii ) <= 1;
out_neighbors  = true(ceil(sqrt(2*size(temp_distances,2)))); 
out_neighbors(tril(out_neighbors ,-1)) = temp_distances;
out_neighbors  = out_neighbors & out_neighbors'; 

end






%% Mahalanobis distance and testing and comparing processing times.
% repeatitions = 100;
% totalTime    = [0,0]
% 
% for i = 1:repeatitions
%   t = tic;
%   temp = pdist(in_data_vector, 'mahalanobis', diag(in_radii.^2)) >= 1;
%   foutA = squareform(temp);
%   totalTime(1) = totalTime(1) + toc(t);
%   t = tic;
%   temp = pdist(in_data_vector, 'seuclidean', in_radii ) >= 1;
%   Z = true(ceil(sqrt(2*size(temp,2)))); 
%   Z(tril(Z,-1)) = temp;
%   foutB = Z & Z'; 
%   totalTime(2) = totalTime(2) + toc(t);
% end
% 
% if(foutA ~= foutB)
%     error('LOGIC ERROR: RESULTS ARE DIFFERENT');
% end
% 
% disp(totalTime / repeatitions);
%%
