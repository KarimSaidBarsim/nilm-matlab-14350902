

X = - 3 + (2*3) * rand(13333,2);                        % Generate data
nghb_mat = fixed_radius_near_neighbor(X, [2,3]);        % Test function
[~,selected_index] = min(sum(X.*X,2));                  % Select middle point
nghb_vec = nghb_mat(selected_index,:);                  % Select neighbors
selected_samples = X(nghb_vec,:);                       % Extract neighbors
figure(1);  hold off;                                   % Plotting ...
plot(X(:,1), X(:,2), 'bo'); hold on                     % Plot data in blue dots.
plot(selected_samples(:,1),selected_samples(:,2),'ro'); % Plot samples surrounding the origin.
plot(X(selected_index,1),X(selected_index,2),'gs');     % Highligh origin.
grid minor;