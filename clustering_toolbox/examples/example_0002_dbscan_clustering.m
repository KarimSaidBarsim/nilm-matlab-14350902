

%% ============================================================================================================ Generating data
samples_per_cluster = 3000;
A = normrnd(133,4,samples_per_cluster,2)+[zeros(samples_per_cluster,1),ones(samples_per_cluster,1)* 0];
B = normrnd(111,4,samples_per_cluster,2)+[zeros(samples_per_cluster,1),ones(samples_per_cluster,1)*22];
C = normrnd( 89,4,samples_per_cluster,2)+[zeros(samples_per_cluster,1),ones(samples_per_cluster,1)*44];
D = normrnd( 67,4,samples_per_cluster,2)+[zeros(samples_per_cluster,1),ones(samples_per_cluster,1)*66];
E = [linspace( 45,155,samples_per_cluster)',normrnd(0,2,samples_per_cluster,1)+110];
F = [linspace( 45,155,samples_per_cluster)',normrnd(0,2,samples_per_cluster,1)+155];
G = [normrnd(0,2,samples_per_cluster,1)+ 45,linspace( 90,175,samples_per_cluster)'];
H = [normrnd(0,2,samples_per_cluster,1)+155,linspace( 90,175,samples_per_cluster)'];
datamat = [A;B;C;D;E;F;G;H];
datamat(randperm(size(datamat,1)),:) = datamat(:,:);                                                            % Randomizing order


% [out_clustering, out_classes] = dbscan(datamat, 100, [2 2]);%3, 100);
[out_clustering, out_classes] = dbscan_clustering(datamat, 3, 100);
disp(['#detected clusters = ' num2str(max(out_clustering))]);
return

%% ============================================================================================================ Plotting 
fontSizeLabels = 9;
current_time   = now;

figure(1), hold off;
plot(A(:,1), A(:,2),'Marker','.','LineStyle','none','Color',rgb('Magenta')); hold on;
plot(B(:,1), B(:,2),'Marker','.','LineStyle','none','Color',rgb('Red'))    ; hold on;
plot(C(:,1), C(:,2),'Marker','.','LineStyle','none','Color',rgb('Lime'))   ; hold on;
plot(D(:,1), D(:,2),'Marker','.','LineStyle','none','Color',rgb('Gold'))   ; hold on;
plot(E(:,1), E(:,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))  ; hold on;
plot(F(:,1), F(:,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))  ; hold on;
plot(G(:,1), G(:,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))  ; hold on;
plot(H(:,1), H(:,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))  ; hold on;
grid minor
set(get(gca,'Title'),'String','Original data','Interpreter', 'latex','FontSize', fontSizeLabels);
set(gcf,'Color','w'); pause(1);
export_fig(fullfile(pwd,'clustering_toolbox','figures',...
    [datestr(current_time,'yyyymmddHHMMSSFFF'),'_dbscan_clustering_test_original_data.png']),'-painters','-m3');
axXLim = get(gca,'XLim');
axYLim = get(gca,'YLim');

figure(2), hold off; %---------------------------------------------------------------------- Cores only
if(max(out_clustering)>=1),plot(datamat(out_clustering==1&out_classes==1,1),datamat(out_clustering==1&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Red'))      ; hold on; end
if(max(out_clustering)>=2),plot(datamat(out_clustering==2&out_classes==1,1),datamat(out_clustering==2&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Lime'))     ; hold on; end
if(max(out_clustering)>=3),plot(datamat(out_clustering==3&out_classes==1,1),datamat(out_clustering==3&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Gold'))     ; hold on; end
if(max(out_clustering)>=4),plot(datamat(out_clustering==4&out_classes==1,1),datamat(out_clustering==4&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Blue'))     ; hold on; end
if(max(out_clustering)>=5),plot(datamat(out_clustering==5&out_classes==1,1),datamat(out_clustering==5&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Cyan'))     ; hold on; end
if(max(out_clustering)>=6),plot(datamat(out_clustering==6&out_classes==1,1),datamat(out_clustering==6&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))    ; hold on; end
if(max(out_clustering)>=7),plot(datamat(out_clustering==7&out_classes==1,1),datamat(out_clustering==7&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Magenta'))  ; hold on; end
if(max(out_clustering)>=8),plot(datamat(out_clustering==8&out_classes==1,1),datamat(out_clustering==8&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('DarkCyan')) ; hold on; end
if(max(out_clustering)>=9),plot(datamat(out_clustering==9&out_classes==1,1),datamat(out_clustering==9&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('DarkGreen')); hold on; end
grid on
set(gca,'XLim',axXLim); set(gca,'YLim',axYLim);
set(get(gca,'Title'),'String','Clusters'' cores only','Interpreter', 'latex','FontSize', fontSizeLabels);
set(gcf,'Color','w'); pause(1);
export_fig(fullfile(pwd,'clustering_toolbox','figures',...
    [datestr(current_time,'yyyymmddHHMMSSFFF'),'_dbscan_clustering_test_cores_only.png']),'-painters','-m3');

figure(3), hold off; %---------------------------------------------------------------------- Borders only
if(max(out_clustering)>=1),plot(datamat(out_clustering==1&out_classes==0,1),datamat(out_clustering==1&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Red'))      ; hold on; end
if(max(out_clustering)>=2),plot(datamat(out_clustering==2&out_classes==0,1),datamat(out_clustering==2&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Lime'))     ; hold on; end
if(max(out_clustering)>=3),plot(datamat(out_clustering==3&out_classes==0,1),datamat(out_clustering==3&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Gold'))     ; hold on; end
if(max(out_clustering)>=4),plot(datamat(out_clustering==4&out_classes==0,1),datamat(out_clustering==4&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Blue'))     ; hold on; end
if(max(out_clustering)>=5),plot(datamat(out_clustering==5&out_classes==0,1),datamat(out_clustering==5&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Cyan'))     ; hold on; end
if(max(out_clustering)>=6),plot(datamat(out_clustering==6&out_classes==0,1),datamat(out_clustering==6&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))    ; hold on; end
if(max(out_clustering)>=7),plot(datamat(out_clustering==7&out_classes==0,1),datamat(out_clustering==7&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Magenta'))  ; hold on; end
if(max(out_clustering)>=8),plot(datamat(out_clustering==8&out_classes==0,1),datamat(out_clustering==8&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('DarkCyan')) ; hold on; end
if(max(out_clustering)>=9),plot(datamat(out_clustering==9&out_classes==0,1),datamat(out_clustering==9&out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('DarkGreen')); hold on; end
grid on
set(gca,'XLim',axXLim); set(gca,'YLim',axYLim);
set(get(gca,'Title'),'String','Clusters'' borders only','Interpreter', 'latex','FontSize', fontSizeLabels);
set(gcf,'Color','w'); pause(1);
export_fig(fullfile(pwd,'clustering_toolbox','figures',...
    [datestr(current_time,'yyyymmddHHMMSSFFF'),'_dbscan_clustering_test_borders_only.png']),'-painters','-m3');

figure(4), hold off; %---------------------------------------------------------------------- Cores with black borders
                           plot(datamat(out_clustering< 1|out_classes==0,1),datamat(out_clustering< 1|out_classes==0,2),'Marker','.','LineStyle','none','Color',rgb('Black'))    ; hold on;
if(max(out_clustering)>=1),plot(datamat(out_clustering==1&out_classes==1,1),datamat(out_clustering==1&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Magenta'))  ; hold on; end
if(max(out_clustering)>=2),plot(datamat(out_clustering==2&out_classes==1,1),datamat(out_clustering==2&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Red'))      ; hold on; end
if(max(out_clustering)>=3),plot(datamat(out_clustering==3&out_classes==1,1),datamat(out_clustering==3&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Lime'))     ; hold on; end
if(max(out_clustering)>=4),plot(datamat(out_clustering==4&out_classes==1,1),datamat(out_clustering==4&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Gold'))     ; hold on; end
if(max(out_clustering)>=5),plot(datamat(out_clustering==5&out_classes==1,1),datamat(out_clustering==5&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))    ; hold on; end
if(max(out_clustering)>=6),plot(datamat(out_clustering==6&out_classes==1,1),datamat(out_clustering==6&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Blue'))     ; hold on; end
if(max(out_clustering)>=7),plot(datamat(out_clustering==7&out_classes==1,1),datamat(out_clustering==7&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('Cyan'))     ; hold on; end
if(max(out_clustering)>=8),plot(datamat(out_clustering==8&out_classes==1,1),datamat(out_clustering==8&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('DarkCyan')) ; hold on; end
if(max(out_clustering)>=9),plot(datamat(out_clustering==9&out_classes==1,1),datamat(out_clustering==9&out_classes==1,2),'Marker','.','LineStyle','none','Color',rgb('DarkGreen')); hold on; end
grid on
set(gca,'XLim',axXLim); set(gca,'YLim',axYLim);
set(get(gca,'Title'),'String','Clusters'' cores and black borders','Interpreter', 'latex','FontSize', fontSizeLabels);
set(gcf,'Color','w'); pause(1);
export_fig(fullfile(pwd,'clustering_toolbox','figures',...
    [datestr(current_time,'yyyymmddHHMMSSFFF'),'_dbscan_clustering_test_cores_black_borders.png']),'-painters','-m3');


figure(5), hold off; %---------------------------------------------------------------------- Cores with borders
                           plot(datamat(out_clustering< 1               ,1),datamat(out_clustering< 1               ,2),'Marker','.','LineStyle','none','Color',rgb('Black'))    ; hold on;
if(max(out_clustering)>=1),plot(datamat(out_clustering==1               ,1),datamat(out_clustering==1               ,2),'Marker','.','LineStyle','none','Color',rgb('Red'))      ; hold on; end
if(max(out_clustering)>=2),plot(datamat(out_clustering==2               ,1),datamat(out_clustering==2               ,2),'Marker','.','LineStyle','none','Color',rgb('Lime'))     ; hold on; end
if(max(out_clustering)>=3),plot(datamat(out_clustering==3               ,1),datamat(out_clustering==3               ,2),'Marker','.','LineStyle','none','Color',rgb('Gold'))     ; hold on; end
if(max(out_clustering)>=4),plot(datamat(out_clustering==4               ,1),datamat(out_clustering==4               ,2),'Marker','.','LineStyle','none','Color',rgb('Blue'))     ; hold on; end
if(max(out_clustering)>=5),plot(datamat(out_clustering==5               ,1),datamat(out_clustering==5               ,2),'Marker','.','LineStyle','none','Color',rgb('Cyan'))     ; hold on; end
if(max(out_clustering)>=6),plot(datamat(out_clustering==6               ,1),datamat(out_clustering==6               ,2),'Marker','.','LineStyle','none','Color',rgb('Brown'))    ; hold on; end
if(max(out_clustering)>=7),plot(datamat(out_clustering==7               ,1),datamat(out_clustering==7               ,2),'Marker','.','LineStyle','none','Color',rgb('Magenta'))  ; hold on; end
if(max(out_clustering)>=8),plot(datamat(out_clustering==8               ,1),datamat(out_clustering==8               ,2),'Marker','.','LineStyle','none','Color',rgb('DarkCyan')) ; hold on; end
if(max(out_clustering)>=9),plot(datamat(out_clustering==9               ,1),datamat(out_clustering==9               ,2),'Marker','.','LineStyle','none','Color',rgb('DarkGreen')); hold on; end
grid on
set(gca,'XLim',axXLim); set(gca,'YLim',axYLim);
set(get(gca,'Title'),'String','Clusters'' cores and borders','Interpreter', 'latex','FontSize', fontSizeLabels);
set(gcf,'Color','w'); pause(1);
export_fig(fullfile(pwd,'clustering_toolbox','figures',...
    [datestr(current_time,'yyyymmddHHMMSSFFF'),'_dbscan_clustering_test_cores_borders.png']),'-painters','-m3');