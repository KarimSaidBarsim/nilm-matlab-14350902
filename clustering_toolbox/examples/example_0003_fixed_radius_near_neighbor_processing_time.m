
signal_lengthes = 10:50:10000;
processing_time = zeros(3,numel(signal_lengthes));


%% ============================================================= 1D
textprogressbar('Processing 1D signal: ');
for counter = 1:numel(signal_lengthes)
    X = rand(signal_lengthes(counter),1);
    t = tic;
    fixed_radius_near_neighbor(X, 1);    
    processing_time(counter,1) = toc(t);
    textprogressbar(100 * counter / numel(signal_lengthes));
end
textprogressbar(' DONE');
%% ============================================================= 2D
textprogressbar('Processing 2D signal: ');
for counter = 1:numel(signal_lengthes)
    X = rand(signal_lengthes(counter),2);
    t = tic;
    fixed_radius_near_neighbor(X, [1 1]);    
    processing_time(counter,2) = toc(t);
    textprogressbar(100 * counter / numel(signal_lengthes));
end
textprogressbar(' DONE');
%% ============================================================= 3D
textprogressbar('Processing 3D signal: ');
for counter = 1:numel(signal_lengthes)
    X = rand(signal_lengthes(counter),3);
    t = tic;
    fixed_radius_near_neighbor(X, [1 1 1]);    
    processing_time(counter,3) = toc(t);
    textprogressbar(100 * counter / numel(signal_lengthes));
end
textprogressbar(' DONE');


%% ============================================================= Plotting
current_figure = figure;
fontSizeLabels = 9;
fontSizeTicks  = 7;

plot(signal_lengthes, processing_time);
legend({'1D','2D','3D'}, 'Location', 'NorthWest');
set(gca, 'YLim', [0, max(max(processing_time))+0.1]);
grid minor;

set(gca, 'FontSize', fontSizeTicks);
set(get(gca, 'XLabel'),'String','Signal length [sample]','Interpreter','latex','FontSize', fontSizeLabels);
set(get(gca, 'YLabel'),'String','Processing time [second]','Interpreter','latex','FontSize', fontSizeLabels);
set(get(gca,'Title'),'String','Performance of the \texttt{fixed\_radius\_near\_neighbor} function',...
    'Interpreter', 'latex','FontSize', fontSizeLabels);

set(current_figure,'Color','w'); pause(1);
export_fig(fullfile(pwd,'clustering_toolbox','figures',...
    [datestr(now,'yyyymmddHHMMSSFFF'),'_fixed_radius_near_neighbor_processing_time.png']),'-painters','-m3');
