function [ out_clustering_vec, out_classes_vec ] = dbscan_clustering( in_data_mat, in_eps, in_min_pts )

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DBSCAN_CLUSTERING is an implementation of the Density-Based Spatial Clustering of Applications with Noise
% (DBSCAN) scheme.
%
%--------------------------------------------------------------------------------------------------------------
% INPUTS:
%   - in_data_mat : [MxN Matrix,REAL] An MxN matrix where each column is a random variable (or a dimension)
%                    and each row is a data sample.
%   - in_min_pts  : [SCALAR,REAL] The minimum number of samples to define a core-point. It is also more or less
%                    the minimum number of samples in a cluster. However, the undeterministic behavior of
%                    border clustering.
%   - in_eps      : [1xN Matrix,REAL] A vector of radii (normalization coeffecients or standard deviations)
%                    of each random variable in the N-dimensions.
%--------------------------------------------------------------------------------------------------------------
% OUTPUTS:
%   - out_clustering_vec : [1xM Vector,REAL] A row vector representing the cluster index of each sample.
%                           Positive integers are cluster indices (for both core and border samples) while zero 
%                           represents outliers/noise. POSITIVE = cluster_index, ZERO = noise/outliers.
%   - out_classes_vec    : [1xM Vector,LOGIC] A row vector representing the class of each data sample in the 
%                           input vector. CORE = 1,  BORDER = -1.
%--------------------------------------------------------------------------------------------------------------
% NOTES:
%   - Standardized (normalized) Euclidean distances is used. This is because it is slightly faster then
%       Mahalanobis distance.
%--------------------------------------------------------------------------------------------------------------
% REQUIRES:
%   - fixed_radius_near_neighbor.m
%--------------------------------------------------------------------------------------------------------------
% EXAMPLES:
%      - X = rand(1000, 3);
%        R = rand(1,    3);
%        S = FIXED_RADIUS_NEAR_NEIGHBOR(X,R);
%--------------------------------------------------------------------------------------------------------------
% created by: Karim Said Barsim
% DATE: 1435-09-03
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% ============================================================================================================ Parsing inputs
in_eps = in_eps(:)';                                                                                            % Force column-vector.
if(numel(in_eps) ~= size(in_data_mat, 2))
    in_eps = in_eps(ones(1, size(in_data_mat, 2)));                                                             % Scalar expansion.
end

%% ============================================================================================================ Parsing inputs
% Solving the Fixed-Radius Near Neighbor algorithm. This is the performance bottle-neck of this neck.
% (Performance = Processing time)
out_neighbors_mat   = fixed_radius_near_neighbor( in_data_mat, in_eps );
sum_neighbors       = sum(out_neighbors_mat, 1);                                                                % #neighbors around each sample.
out_classes_vec     = sum_neighbors >= in_min_pts;                                                              % Core samples.
out_clustering_vec  = zeros(size(out_classes_vec));                                                             % Memory pre-allocation.

%% ============================================================================================================ Core point detection ...
outer_cluster_counter = 0;
for sample_counter = 1:numel(out_clustering_vec)
    if(out_clustering_vec(sample_counter) ~= 0 || out_classes_vec(sample_counter) ~= 1), continue; end          % Sample already clustered or it is not a core one.
    if(out_classes_vec(sample_counter))                                                                         % If it is an unclustered core-sample.
        outer_cluster_counter = outer_cluster_counter + 1;                                                      % New cluster is detected.
%         func_expand_cluster_iterative_row(...
%             out_neighbors_mat(sample_counter,:) & out_classes_vec, outer_cluster_counter);                      % Then expand current cluster.
        func_expand_cluster_recursive(sample_counter, outer_cluster_counter);                                           % Then expand current cluster.
    end
end

%% ============================================================================================================ Expanding densities/clusters (Recursively) ...
    function [] = func_expand_cluster_recursive( sample_index, current_cluster_index )
        func_expand_cluster_recursive_helped( out_neighbors_mat(sample_index,:) & out_classes_vec, current_cluster_index );
    end

    function [] = func_expand_cluster_recursive_helped( surrounding_samples, current_cluster_index )
        if( ~any(surrounding_samples) )
            return;
        end
        out_clustering_vec(surrounding_samples) = current_cluster_index;                                        % All samples within eps-radius belong to the current cluster.
        func_expand_cluster_recursive_helped( ...
            any(out_neighbors_mat(surrounding_samples,:),1) & out_classes_vec & out_clustering_vec == 0,...
            current_cluster_index);
    end

%% ============================================================================================================ Expanding densities/clusters (Iteratively) ...
    function [] = func_expand_cluster_iterative_row( new_surrounding_samples, current_cluster_index )
        while(any(new_surrounding_samples))
            out_clustering_vec(new_surrounding_samples) = current_cluster_index;
            new_surrounding_samples = any(out_neighbors_mat(new_surrounding_samples,:),1) & out_classes_vec & (out_clustering_vec == 0);
        end
        
%         old_surrounding_samples = new_surrounding_samples;
%         new_surrounding_samples = any(out_neighbors_mat(new_surrounding_samples,:),1) & out_classes_vec;        % Initialize expansion
%         while(~isequal(new_surrounding_samples,old_surrounding_samples))                                        % As long as expansion works ...
%             old_surrounding_samples = new_surrounding_samples;
%             new_surrounding_samples = ...
%               any(out_neighbors_mat(new_surrounding_samples,:),1) & out_classes_vec | old_surrounding_samples;  % ... then expand from core points only.
%         end
%         %out_clustering_vec(new_surrounding_samples) = current_cluster_index;                                    % Cluster core points
%         out_clustering_vec(any(out_neighbors_mat(new_surrounding_samples,:),1)) = current_cluster_index;        % Cluster borders
    end

% For performance (i.e. processing time) comparison and for applications with low memory.
%     function [] = func_expand_cluster_iterative_col( new_surrounding_samples, current_cluster_index )
%         old_surrounding_samples = new_surrounding_samples;
%         new_surrounding_samples = any(out_neighbors_mat(:,new_surrounding_samples),2) & out_classes_vec;        % Initialize expansion
%         while(~isequal(new_surrounding_samples,old_surrounding_samples))                                        % As long as expansion works ...
%             old_surrounding_samples = new_surrounding_samples;
%             new_surrounding_samples = ...
%                 any(out_neighbors_mat(:,new_surrounding_samples),2) & out_classes_vec | old_surrounding_samples;% ... then expand from core points only.
%         end
%         out_clustering_vec(new_surrounding_samples) = current_cluster_index;                                    % Cluster core points
%         out_clustering_vec(any(out_neighbors_mat(:,new_surrounding_samples),2)) = current_cluster_index;        % Cluster borders
%     end



end